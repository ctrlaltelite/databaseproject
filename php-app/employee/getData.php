<?php
include '../dbConnect.inc';
include '../dbDetails.inc';
//Check that all fields are filled.
if(isset($_POST['ssn']) && isset($_POST['name']) && isset($_POST['address']) && isset($_POST['city'])
    && isset($_POST['state']) && isset($_POST['state']) && isset($_POST['zipcode']) && isset($_POST['phone'])
    && isset($_POST['cell']) && isset($_POST['email'])) {
    //Declare variables, assign values.
    $ssn = $_POST['ssn'];
    $name = $_POST['name'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zipcode = $_POST['zipcode'];
    $phone = $_POST['phone'];
    $cell = $_POST['cell'];
    $email = $_POST['email'];

    //sees if the connection to the database was successful before continuing
    $connection = dbConnect($server, $userid, $password, $database, $port);
    if($connection){ //Connection successful. Prepare to insert data.
        $exists = mysqli_query($connection,'SELECT 1 FROM `EMPLOYEES` LIMIT 1');
        if($exists == FALSE){
            //Table does not exist. Create it.
            mysqli_query($connection, 'CREATE TABLE EMPLOYEES (
            employee_id BIGINT NOT NULL AUTO_INCREMENT
            ,employee_ssn VARCHAR(20) UNIQUE NOT NULL
            ,employee_name VARCHAR(50) NOT NULL
            ,employee_address VARCHAR(50) NOT NULL
            ,employee_city VARCHAR(50) NOT NULL
            ,employee_state VARCHAR(50) NOT NULL
            ,employee_zipcode VARCHAR(20) NOT NULL
            ,employee_homephone VARCHAR(20) NOT NULL
            ,employee_cellphone VARCHAR(20) NOT NULL
            ,employee_email VARCHAR(50) NOT NULL
            ,employee_commissionpercent DECIMAL(4,2) NOT NULL DEFAULT 2
            ,employee_salescount INT NOT NULL DEFAULT 0
            ,employee_salesamount DECIMAL(13,2) NOT NULL DEFAULT 0
            ,PRIMARY KEY(employee_id)
            )');
        }

        //Prepare the insert statement.
        $sql = "INSERT INTO EMPLOYEES(employee_ssn, employee_name, employee_address, employee_city, 
                      employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount) 
                      VALUES ('$ssn', '$name', '$address', '$city', '$state', '$zipcode', '$phone', '$cell', '$email', '2.00', '0', '0')";
        if(mysqli_query($connection, $sql)){
            //The insert was successful. Go back to the homepage.
            //echo '<script type="text/javascript">alert("Employee successfully added. Now redirecting to homepage.")</script>';
            header("Location: /index.php", true, 301);
            exit();
        }
        else{ //Insert statement failed.
            echo "Error: " . mysqli_error($connection);
        }
    }
    else{ //Connection failed. Display error.
        die("Connection failed: " . mysqli_connect_error());
    }
}