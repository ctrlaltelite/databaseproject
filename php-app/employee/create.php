<?php

?>
<!DOCTYPE html>
	<html lang = "en">
		<head>
			<meta charset="utf-8"/>
			<title>Employee Entry Form</title>
			<link href="/form.css" type="text/css" rel="stylesheet"/>
        </head>
		<body>
        <?php
        include '../header.php'
        ?>
        <h1>Employee Entry Form</h1>
			<form class="form" action="getData.php" method="post">
				<div>
                    <div class="form-field">
                        <label class="form-label" id="lblssn" for="ssn">Social Security #</label>
                        <input class="form-input" id="ssn" name="ssn" type="text"/>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lblname" for="name">Name</label>
					    <input class="form-input" id="name" name="name" type="text"/>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lbladdress" for="address">Street Address</label>
					    <input class="form-input" id="address" name="address" type="text"/>
                    </div>
                    <div class="form-field">
                        <label class="form-label" id="lblcity" for="city">City</label>
					    <input class="form-input" id="city" name="city" type="text"/>
                    </div>
                    <div class="form-field">
                    <label class="form-label" id="lblstate" for="state">State</label>
                    <select class="form-input" name="state">
                        <option value="alaska">Alaska</option>
                        <option value="arizona">Arizona</option>
                        <option value="arkansas">Arkansas</option>
                        <option value="california">California</option>
                        <option value="colorado">Colorado</option>
                        <option value="connecticut">Connecticut</option>
                        <option value="delaware">Delaware</option>
                        <option value="flordia">Florida</option>
                        <option value="georgia">Georgia</option>
                        <option value="hawaii">Hawaii</option>
                        <option value="idaho">Idaho</option>
                        <option value="illinois">Illinois</option>
                        <option value="indiana">Indiana</option>
                        <option value="iowa">Iowa</option>
                        <option value="kansas">Kansas</option>
                        <option value="kentucky">Kentucky</option>
                        <option value="louisiana">Louisiana</option>
                        <option value="maine">Maine</option>
                        <option value="maryland">Maryland</option>
                        <option value="massachusetts">Massachusetts</option>
                        <option value="michigan">Michigan</option>
                        <option value="minnesota">Minnesota</option>
                        <option value="mississippi">Mississippi</option>
                        <option value="missouri">Missouri</option>
                        <option value="montana">Montana</option>
                        <option value="nebraska">Nebraska</option>
                        <option value="nevada">Nevada</option>
                        <option value="newhampshire">New Hampshire</option>
                        <option value="newjersey">New Jersey</option>
                        <option value="newmexico">New Mexico</option>
                        <option value="newyork">New York</option>
                        <option value="northcarolina">North Carolina</option>
                        <option value="northdakota">North Dakota</option>
                        <option value="ohio">Ohio</option>
                        <option value="oklahoma">Oklahoma</option>
                        <option value="oregon">Oregon</option>
                        <option value="pennsylvania">Pennsylvania</option>
                        <option value="rhodeisland">Rhode Island</option>
                        <option value="southcarolina">South Carolina</option>
                        <option value="southdakota">South Dakota</option>
                        <option value="tennessee">Tennessee</option>
                        <option value="texas">Texas</option>
                        <option value="utah">Utah</option>
                        <option value="vermont">Vermont</option>
                        <option value="virginia">Virginia</option>
                        <option value="washington">Washington</option>
                        <option value="westvirgina">West Virginia</option>
                        <option value="wisconsin">Wisconsin</option>
                        <option value="wyoming">Wyoming</option>
                    </select>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lblzipcode" for="zipcode">Zip Code</label>
					    <input class="form-input" id="zipcode" name="zipcode" type="text"/>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lblphone" for="phone">Home Phone</label>
					    <input class="form-input" id="phone" name="phone" type="tel"/>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lblcell" for="cell">Cell</label>
					    <input class="form-input" id="cell" name="cell" type="tel"/>
                    </div>
                    <div class="form-field">
					    <label class="form-label" id="lblemail" for="email">E-mail</label>
					    <input class="form-input" id="email" name="email" type="email"/>
                    </div>

				</div>
                <button type="submit" class="button submit">Create</button>
			</form>
        <?php
        include '../footer.php'
        ?>
		</body>
	</html>
