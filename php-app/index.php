<html>
<head>
    <meta charset="utf-8"/>
    <title>Index</title>
    <link href="/form.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<?php
    include 'header.php'
?>
<h1>Entry Forms</h1>
<ul>
    <li><a href="customer/create.php">Customer Entry Form</a></li>
    <li><a href="employee/create.php">Employee Entry Form</a></li>
</ul>
<?php
include 'footer.php'
?>
</body>
</html>


