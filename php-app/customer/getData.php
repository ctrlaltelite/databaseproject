<?php
include '../dbConnect.inc';
include '../dbDetails.inc';
//Verify that all fields are filled before continuing.
if(isset($_POST['name']) && isset($_POST['cell']) && isset($_POST['email']) &&
    isset($_POST['address']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zipcode'])){
    //Declare variables, assign them the values given from the form.
    $name = $_POST['name'];
    $cell = $_POST['cell'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zipcode = $_POST['zipcode'];
    //Open a connection to the server.
    $connection = dbConnect($server, $userid, $password, $database, $port);
    //If successful, proceed.
    if($connection){
        //Check if the table exists first.
        $exists = mysqli_query($connection,'SELECT 1 FROM `CUSTOMERS` LIMIT 1');
        if($exists == FALSE){
            mysqli_query($connection, 'CREATE TABLE CUSTOMERS (
            customer_id BIGINT NOT NULL AUTO_INCREMENT
            ,customer_name VARCHAR(50) NOT NULL
            ,customer_address VARCHAR(50) NOT NULL
            ,customer_city VARCHAR(50) NOT NULL
            ,customer_state VARCHAR(50) NOT NULL
            ,customer_zipcode VARCHAR(20) NOT NULL
            ,customer_phone VARCHAR(20) NOT NULL
            ,customer_creditbalance DECIMAL(13,2) NOT NULL
            ,PRIMARY KEY (customer_id)
        )');
        }
        //Prepare the SQL statement for inserting the data.
        $sql = "INSERT INTO CUSTOMERS(customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
                VALUES ('$name', '$address', '$city', '$state', '$zipcode', '$cell', '0.00')";

        if(mysqli_query($connection, $sql)){ //Query was successful. Redirect back to homepage.
            //echo '<script type="text/javascript">alert("Customer successfully added. Now redirecting to homepage.")</script>';
            header("Location: /index.php", true, 301);
            exit();
        }
        else{ //Insert statement failed.
            echo "Error: " . mysqli_error($connection);
        }
    }
    else{ //Could not open the connection. Show an error.
        die("Connection failed: " . mysqli_connect_error());
    }
}

