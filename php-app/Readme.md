-### Example startup for php server from command line (UNIX)
/usr/bin/php -S localhost:8080 -t /path/to/Deliverable5

# Git Commands
Documentation: https://git-scm.com/docs

Paul's quicker reference
- add
    - Usage: git add [File(s) or directory]
    - Adds file(s) to git tree for tracking. Normally PHPStorm will do this for you with a popup
- stage
    - Usage: git stage [--all] [File(s) or directory] 
    - Stages changes in files. If for some reason your changes aren't committed use this with the --all flag to add it 
- commit 
    - Usage: git commit [-a] [-m "Message"]
- push
    - Pushes changes to the remote server
- pull
    - Pulls changes from remote server
- merge
    - Will explain more if needed
- checkout
    - Will explain more if needed 
    
# Docker Commands
- docker-compose up -d
    - Starts up the docker instance. Must be in the scope of the compose files.
- docker-compose down
    - Kills the instance of the docker. DO this before starting it up if you made changes.