# Database Project - Ctrl Alt Elite

Welcome to the repository containing our final project for INFO 4407.

All files regarding our website and input forms, as well as all of our **PHP** 
code can be found in the folder _php-app_.

All files containing **MySQL** code can be found in the folder _sql_. This includes queries, 
stored procedures, and triggers, as well as other scripts such as those used to drop 
and recreate the database as well as insert data into the tables. 

* [Website URL](http://4407.paulgrahek.us)
* [Database URL](http://db.4407.paulgrahek.us)

Contributors:
* Tyler Kness-Miller
* Victoria Steckline
* Paul Grahek
* Caitlyn Bailey

