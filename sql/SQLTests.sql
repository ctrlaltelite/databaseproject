use db4407;
#
#   Queries Test
#

CALL prQ1();
CALL prQ2();
CALL prQ3();
# CALL prQ4();
# Query 4 Fix
SELECT c.customer_name, e.equipment_serialnumber, e.model_id, pc.model_description, pa.purchase_datetime
FROM PURCHASEAGREEMENTS pa, CUSTOMERS c, LINEITEMS li, PIECEOFEQUIPMENT e, PRODUCTCATEGORIES pc
WHERE pa.customer_id = c.customer_id
  AND li.purchaseagreement_id = pa.purchaseagreement_id
  AND e.equipment_id = li.equipment_id
  AND e.model_id = pc.model_id
  AND DATE(pa.purchase_datetime) = '2010-10-6';

CALL prQ5();
CALL prQ6a();
CALL prQ6b();
CALL prQ7();
CALL prQ8('Customer1');
CALL prQ9();
CALL prQ10();


#
# Stored Procedure Testing
#
CALL prCheckReorderNecessary();
# Good CC
SELECT prValidateCreditCard('5268080043376894');
# Bad CC
SELECT prValidateCreditCard('5268080043376893');


#
#   Triggers Testing
#

# Inventory Update Verification
SET @equipment_id = 1;
SET @model_id = 1;
UPDATE PRODUCTCATEGORIES
    SET model_inventorycount = model_reorderquantity + 1
        WHERE model_id = @model_id;
CALL prCheckReorderNecessary();
SELECT * FROM PRODUCTCATEGORIES
    WHERE model_id = @model_id;
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
    VALUES (1,@equipment_id);
SELECT * FROM PRODUCTCATEGORIES pc
    WHERE pc.model_id = @model_id;

# Purchase Agreement Credit Card Verification (GOOD)
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
    VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',2,4);
# Purchase Agreement Credit Card Verification (BAD)
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
    VALUES ('2018-05-05 12:30:00','creditcard','5268080043376893',2,4);