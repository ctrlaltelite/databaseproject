# noinspection SqlNoDataSourceInspectionForFile

USE db4407;

-- Query 1
-- List the customer name, customer number, purchase agreement number, and purchase total for each purchase agreement.
-- Give the purchase total a meaningful name.
DELIMITER //
CREATE PROCEDURE prQ1()
BEGIN
    SELECT pa.customer_id, c.customer_name, pa.purchaseagreement_id, (SUM(poe.equipment_saleprice)* (1 + 0.05)) AS purchase_total
      FROM PURCHASEAGREEMENTS pa, CUSTOMERS c, LINEITEMS li, PIECEOFEQUIPMENT poe
        WHERE pa.customer_id = c.customer_id
          AND li.purchaseagreement_id = pa.purchaseagreement_id
          AND li.equipment_id = poe.equipment_id
            GROUP BY pa.purchaseagreement_id;
END; //
DELIMITER ;


-- Query 2
-- List the model description and manufacturer for all products whose manufacturer name starts with an "A" and has a third letter "r".
-- (Your answer should include Aeros, Airwave, and Airush.)
-- (When typing this query, copy and paste it into phpMyAdmin, then retype the apostrophe marks)
DELIMITER //
CREATE PROCEDURE prQ2()
BEGIN
SELECT model_description, model_manufacturer
  FROM PRODUCTCATEGORIES
    WHERE model_manufacturer LIKE 'A_r%';
END; //
DELIMITER ;

-- Query 3
-- List all salesman information, including employee#, name, sales count, and sales total (SalesAmount)
-- and commission earned in descending order by commission earned.
DELIMITER //
CREATE PROCEDURE prQ3()
BEGIN
SELECT e.employee_id, e.employee_name, e.employee_salescount, e.employee_salesamount, ROUND(e.employee_salesamount * (e.employee_commissionpercent / 100),2) as commission_earned
  FROM EMPLOYEES e
    ORDER BY commission_earned DESC;
END; //
DELIMITER ;

-- Query 4
-- List the customer name, serial number, model number, model description, and purchase agreement date
-- for all items having a purchase agreement date of October 6, 2010.
-- (When typing this query, copy and paste it into phpMyAdmin, then retype the apostrophe marks)
-- TODO: Fix weirdness in phpMyAdmin
DELIMITER //
CREATE PROCEDURE prQ4()
BEGIN
SELECT c.customer_name, e.equipment_serialnumber, e.model_id, pc.model_description, pa.purchase_datetime
  FROM PURCHASEAGREEMENTS pa, CUSTOMERS c, LINEITEMS li, PIECEOFEQUIPMENT e, PRODUCTCATEGORIES pc
    WHERE pa.customer_id = c.customer_id
      AND li.purchaseagreement_id = pa.purchaseagreement_id
      AND e.equipment_id = li.equipment_id
      AND e.model_id = pc.model_id
      AND DATE(pa.purchase_datetime) = '2010-10-6';

END; //
DELIMITER ;

-- Query 5
-- List all orders that were placed over 1 month ago and have not been received.
-- Include order number, order date placed, order date received, supplier number, supplier name, supplier contact, and supplier phone.
-- List by order date placed from oldest to most recent. INTERVAL is useful here.
DELIMITER //
CREATE PROCEDURE prQ5()
BEGIN
SELECT o.order_id, o.orders_date_made, o.orders_date_received, s.supplier_id, s.supplier_name, s.supplier_contact, s.supplier_phone
    FROM ORDERS o, SUPPLIERS s
        WHERE o.supplier_id = s.supplier_id
            AND o.orders_date_made <= NOW() - INTERVAL 1 MONTH
            AND o.orders_date_received IS NULL
            ORDER BY o.orders_date_made ASC;
END; //
DELIMITER ;

-- Query 6
-- For each product category list the model number, description, and the total number of sales from that category.
-- As a continuation from the previous query (6a), [using a nested query] list the model number, description,
-- and the total number of sales for the product category that has the highest number of sales. SOMEWHAT HARD
-- Doesn’t account for returns
-- 6a View
DELIMITER //
CREATE OR REPLACE VIEW IF NOT EXISTS q6a
AS
SELECT pc.model_id, pc.model_description, COUNT(pe.equipment_id) as total_sales
    FROM PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe, LINEITEMS li
        WHERE pc.model_id = pe.model_id
            AND pe.equipment_id = li.equipment_id
              GROUP BY pc.model_id;
//
DELIMITER ;
-- 6a Query
DELIMITER //
CREATE PROCEDURE prQ6a()
BEGIN
SELECT * FROM q6a;
END; //
DELIMITER ;

-- 6b Query
DELIMITER //
CREATE PROCEDURE prQ6b()
BEGIN
SELECT model_id, model_description, total_sales
    FROM q6a
        WHERE total_sales = (SELECT MAX(total_sales) FROM q6a);
END; //
DELIMITER ;


-- Query 7
-- List the model number, description, and number of suppliers for those product categories that were supplied by multiple suppliers. HARD
-- 7 View
DELIMITER //
CREATE VIEW IF NOT EXISTS q7a
AS
SELECT pc.model_id, pc.model_description, COUNT(pe.supplier_id) AS  'supplier_count'
  FROM PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe, SUPPLIERS s
    WHERE pc.model_id = pe.model_id
      AND pe.supplier_id = s.supplier_id
        GROUP BY pc.model_id;
//
DELIMITER ;
# SELECT * FROM q7a;
-- 7 Query
DELIMITER //
CREATE PROCEDURE prQ7()
BEGIN
SELECT model_id, model_description, supplier_count
    FROM q7a
        WHERE supplier_count > 1;
END; //
DELIMITER ;

-- Query 8
-- Given a customer name, list all credit cards associated with that customer. Include card number, card type (Visa, Mastercard, etc.), and expiration year and month, in the format yyyy/mm. (Arbitrarily select a customer name.)
DELIMITER //
CREATE PROCEDURE prQ8(customer_name VARCHAR(50))
BEGIN
    SELECT c.customer_name, cc.creditcard_number, cc.creditcard_type, (cc.creditcard_expirationyear + '/' + cc.creditcard_expirationmonth) as creditcard_expiration
        FROM CREDITCARDS cc, HOLDS_ACCOUNTS ha, CUSTOMERS c
        WHERE ha.customer_id = c.customer_id
          AND ha.creditcard_number = cc.creditcard_number
          AND c.customer_name = customer_name;
END; //
DELIMITER ;

-- Query 9
-- List all products in inventory that were purchased more than 18 months ago.
-- Display the serial number, purchase date, supplier ID, supplier name, supplier phone number.
DELIMITER //
CREATE PROCEDURE prQ9()
BEGIN
SELECT pe.equipment_serialnumber, pe.equipment_purchasedate, pe.supplier_id, s.supplier_name, s.supplier_phone
    FROM PIECEOFEQUIPMENT pe, SUPPLIERS s
        WHERE pe.supplier_id = s.supplier_id AND pe.equipment_purchasedate <= (NOW() - INTERVAL 18 MONTH);
END; //
DELIMITER ;

-- Query 10
-- List supplier number and supplier name for all suppliers for which there are no current orders.
-- Sort the list in ascending order by supplier name.
DELIMITER //
CREATE PROCEDURE prQ10()
BEGIN
    SELECT DISTINCT s.supplier_id, s.supplier_name
    FROM SUPPLIERS s
    WHERE (SELECT COUNT(s.supplier_id)
           FROM ORDERS oo
           WHERE oo.supplier_id = s.supplier_id
             AND oo.orders_date_received IS NULL) = 0
    ORDER BY supplier_name;
END; //
DELIMITER ;


