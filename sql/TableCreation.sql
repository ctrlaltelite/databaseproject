USE db4407;

-- Customer (Customer#, CustomerName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerPhone, CustomerEmail) CustomerCreditBalance)
CREATE TABLE CUSTOMERS (
    customer_id BIGINT NOT NULL AUTO_INCREMENT
    ,customer_name VARCHAR(50) NOT NULL
    ,customer_address VARCHAR(50) NOT NULL
    ,customer_city VARCHAR(50) NOT NULL
    ,customer_state VARCHAR(50) NOT NULL
    ,customer_zipcode VARCHAR(20) NOT NULL
    ,customer_phone VARCHAR(20) NOT NULL
    ,customer_creditbalance DECIMAL(13,2) NOT NULL
    ,PRIMARY KEY (customer_id)
);

-- Balloon (BalloonRide#, <more>)
CREATE TABLE BALLOONRIDES (
    balloonride_id BIGINT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY (balloonride_id)
    ,balloonride_date DATETIME NOT NULL
);

-- Reservation (BalloonRide#, Customer#)
CREATE TABLE RESERVATIONS (
    balloonride_id BIGINT NOT NULL
    ,customer_id BIGINT NOT NULL
    ,FOREIGN KEY (balloonride_id)
        REFERENCES BALLOONRIDES(balloonride_id)
    ,FOREIGN KEY (customer_id)
        REFERENCES CUSTOMERS(customer_id)
    ,PRIMARY KEY (balloonride_id,customer_id)
);

-- Credit_Card (CreditCard#,     CreditCardType, CreditCardExpirationMonth, CreditCardExpirationYear)
CREATE TABLE CREDITCARDS (
    creditcard_number VARCHAR(16) NOT NULL
    ,creditcard_type VARCHAR(50) NOT NULL
    ,creditcard_expirationmonth SMALLINT NOT NULL
    ,creditcard_expirationyear SMALLINT NOT NULL
    ,PRIMARY KEY(creditcard_number)
);

-- Employee (Employee#, EmployeeSSN, EmployeeName, EmployeeAddress, EmployeeCity, EmployeeState, EmployeeZip, EmployeeHomePhone, EmployeeCellPhone, EmployeeEmail)
CREATE TABLE EMPLOYEES (
    employee_id BIGINT NOT NULL AUTO_INCREMENT
    ,employee_ssn VARCHAR(20) UNIQUE NOT NULL
    ,employee_name VARCHAR(50) NOT NULL
    ,employee_address VARCHAR(50) NOT NULL
    ,employee_city VARCHAR(50) NOT NULL
    ,employee_state VARCHAR(50) NOT NULL
    ,employee_zipcode VARCHAR(20) NOT NULL
    ,employee_homephone VARCHAR(20) NOT NULL
    ,employee_cellphone VARCHAR(20) NOT NULL
    ,employee_email VARCHAR(50) NOT NULL
    ,employee_commissionpercent DECIMAL(4,2) NOT NULL
    ,employee_salescount INT NOT NULL
    ,employee_salesamount DECIMAL(13,2) NOT NULL
    ,PRIMARY KEY(employee_id)
);

-- Purchase_Agreement (PurchaseAgreement#, PurchaseAgreementDate, PurchaseAgreementTime, PurchaseAgreementPaymentType, PurchaseAgreementCardNumberUsed, Customer#, Employee#)
CREATE TABLE PURCHASEAGREEMENTS(
     purchaseagreement_id BIGINT NOT NULL AUTO_INCREMENT
    ,purchase_datetime DATETIME NOT NULL
    ,purchase_paymenttype ENUM('balesofhay','manhours','firstbornchild','creditcard','cash','checks') NOT NULL
    ,purchase_cardnumberused VARCHAR(16) NOT NULL
    ,customer_id BIGINT NOT NULL
    ,employee_id BIGINT NOT NULL
    ,PRIMARY KEY(purchaseagreement_id)
    ,FOREIGN KEY(customer_id)
        REFERENCES CUSTOMERS(customer_id)
    ,FOREIGN KEY(employee_id)
        REFERENCES EMPLOYEES(employee_id)
);

-- Supplier (Supplier#, SupplierName, SupplierAddress, SupplierCity, SupplierState,     SupplierZip, SupplierPhone, SupplierFax, SupplierContact, SupplierEmail)
CREATE TABLE SUPPLIERS(
    supplier_id BIGINT NOT NULL AUTO_INCREMENT
    ,supplier_name VARCHAR(50) NOT NULL
    ,supplier_address VARCHAR(50) NOT NULL
    ,supplier_city VARCHAR(50) NOT NULL
    ,supplier_state VARCHAR(50) NOT NULL
    ,supplier_zip  VARCHAR(20) NOT NULL
    ,supplier_phone VARCHAR(20) NOT NULL
    ,supplier_fax VARCHAR(20) NOT NULL
    ,supplier_contact VARCHAR(50) NOT NULL
    ,supplier_email VARCHAR(50) NOT NULL
    ,PRIMARY KEY(supplier_id)
);


-- Product_Category (Model#, ModelDescription, ModelManufacturer, ModelInventoryCount, ModelReorderQuantity)
CREATE TABLE PRODUCTCATEGORIES(
    model_id BIGINT NOT NULL AUTO_INCREMENT
    ,model_description VARCHAR(100) NOT NULL
    ,model_manufacturer VARCHAR(50) NOT NULL
    ,model_inventorycount INTEGER NOT NULL
    ,model_reorderquantity INTEGER NOT NULL
    ,PRIMARY KEY(model_id)
);

-- Piece_of_Equipment (Serial#, EquipmentPurchasePrice, EquipmentSalePrice, EquipmentPurchaseDate, Model#, Supplier#) EquipmentType, EquipmentDescription)
CREATE TABLE PIECEOFEQUIPMENT(
     equipment_id BIGINT NOT NULL AUTO_INCREMENT
    ,equipment_serialnumber BIGINT NOT NULL
    ,equipment_purchaseprice DECIMAL(13, 2) NOT NULL
    ,equipment_saleprice DECIMAL(13, 2) NOT NULL
    ,equipment_purchasedate DATETIME NOT NULL
    ,model_id BIGINT NOT NULL
    ,supplier_id  BIGINT NOT NULL
    ,PRIMARY KEY(equipment_id)
    ,FOREIGN KEY(model_id)
        REFERENCES PRODUCTCATEGORIES(model_id)
    ,FOREIGN KEY(supplier_id)
        REFERENCES SUPPLIERS(supplier_id)
);

-- Order (Order#, OrderDatePlaced, OrderDateReceived, Supplier#)
CREATE TABLE ORDERS(
    order_id BIGINT NOT NULL AUTO_INCREMENT
    ,orders_date_made DATETIME NOT NULL
    ,orders_date_received DATETIME
    ,supplier_id BIGINT NOT NULL
    ,PRIMARY KEY(order_id)
    ,FOREIGN KEY(supplier_id)
        REFERENCES SUPPLIERS (supplier_id)
);


-- THIS DOESNT HAVE ENOUGH TO IT???
-- Return (Return#, Customer#)
CREATE TABLE RETURNS (
    return_id BIGINT NOT NULL AUTO_INCREMENT
    ,customer_id BIGINT NOT NULL
    ,PRIMARY KEY(return_id)
    ,FOREIGN KEY(customer_id)
        REFERENCES CUSTOMERS(customer_id)
);
-- Holds_Account (Customer#, CreditCard#)
CREATE TABLE HOLDS_ACCOUNTS (
    customer_id BIGINT NOT NULL
    ,creditcard_number VARCHAR(16) NOT NULL
    ,FOREIGN KEY (customer_id)
        REFERENCES CUSTOMERS(customer_id)
    ,FOREIGN KEY (creditcard_number)
        REFERENCES CREDITCARDS(creditcard_number)
);

-- Line_Item (PurchaseAgreement#, Serial#)
CREATE TABLE LINEITEMS (
    purchaseagreement_id BIGINT NOT NULL AUTO_INCREMENT
    ,equipment_id BIGINT NOT NULL
    ,FOREIGN KEY (purchaseagreement_id)
        REFERENCES PURCHASEAGREEMENTS(purchaseagreement_id)
    ,FOREIGN KEY (equipment_id)
        REFERENCES PIECEOFEQUIPMENT(equipment_id)
);

-- Line_Item_Equipment (Order#, Model#, LineItemEquipmentQuantity, LineItemEquipmentPrice)
CREATE TABLE LINEITEMEQUIPMENTS(
    order_id BIGINT NOT NULL
    ,model_id BIGINT NOT NULL
    ,lineitem_equipmentquantity INTEGER NOT NULL
    ,lineitem_equipmentprice DECIMAL(13,2) NOT NULL
    ,FOREIGN KEY(order_id)
        REFERENCES ORDERS(order_id)
    ,FOREIGN KEY(model_id)
        REFERENCES PRODUCTCATEGORIES(model_id)

);

-- Return_Item(Serial#, Return#, ReturnReason)
CREATE TABLE RETURNITEMS(
    equipment_id BIGINT NOT NULL
    , return_id BIGINT NOT NULL
    , return_reason VARCHAR(200) NOT NULL
    ,FOREIGN KEY(equipment_id)
        REFERENCES PIECEOFEQUIPMENT(equipment_id)
    ,FOREIGN KEY(return_id)
        REFERENCES RETURNS(return_id)
);
