use db4407;
# Trigger #1
CREATE TRIGGER sell_inventory AFTER INSERT ON LINEITEMS
  FOR EACH ROW
  UPDATE PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe
  SET pc.model_inventorycount = pc.model_inventorycount - 1,
      pc.model_reordernecessary= !(pc.model_inventorycount-1 > pc.model_reorderquantity)
  WHERE NEW.equipment_id = pe.equipment_id
    AND pe.model_id = pc.model_id;

# Trigger #2
delimiter //
CREATE TRIGGER validateCreditCard_Insert BEFORE INSERT ON PURCHASEAGREEMENTS
  FOR EACH ROW
BEGIN
  DECLARE isValid INT;
  SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
  IF isValid = 0 THEN
    # Bad Credit Card
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
  END IF;
END;//
delimiter ;
delimiter //
CREATE TRIGGER validateCreditCard_Update BEFORE UPDATE ON PURCHASEAGREEMENTS
  FOR EACH ROW
BEGIN
  DECLARE isValid INT;
  IF(NEW.purchase_paymenttype = 'creditcard') THEN
    SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
    IF isValid = 0 THEN
      # Bad Credit Card
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
    END IF;
  END IF;
END;//
delimiter ;

# Testing for Validate
delimiter //
CREATE FUNCTION TESTCCTRIGGER(creditCardNum varchar(16))
    RETURNS INT
BEGIN
    DECLARE isValid INT;
    IF(NEW.purchase_paymenttype = 'creditcard') THEN
        SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
        IF isValid = 0 THEN
            # Bad Credit Card
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
        END IF;
    END IF;
    RETURN 1;
END;//
delimiter ;