DROP DATABASE IF EXISTS db4407;
CREATE DATABASE db4407;
ALTER DATABASE db4407 CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE db4407;

-- Customer (Customer#, CustomerName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerPhone, CustomerEmail) CustomerCreditBalance)
CREATE TABLE CUSTOMERS (
                           customer_id BIGINT NOT NULL AUTO_INCREMENT
    ,customer_name VARCHAR(50) NOT NULL
    ,customer_address VARCHAR(50) NOT NULL
    ,customer_city VARCHAR(50) NOT NULL
    ,customer_state VARCHAR(50) NOT NULL
    ,customer_zipcode VARCHAR(20) NOT NULL
    ,customer_phone VARCHAR(20) NOT NULL
    ,customer_creditbalance DECIMAL(13,2) NOT NULL
    ,PRIMARY KEY (customer_id)
);

-- Balloon (BalloonRide#, <more>)
CREATE TABLE BALLOONRIDES (
                              balloonride_id BIGINT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY (balloonride_id)
    ,balloonride_date DATETIME NOT NULL
);

-- Reservation (BalloonRide#, Customer#)
CREATE TABLE RESERVATIONS (
                              balloonride_id BIGINT NOT NULL
    ,customer_id BIGINT NOT NULL
    ,FOREIGN KEY (balloonride_id)
                                  REFERENCES BALLOONRIDES(balloonride_id)
    ,FOREIGN KEY (customer_id)
                                  REFERENCES CUSTOMERS(customer_id)
    ,PRIMARY KEY (balloonride_id,customer_id)
);

-- Credit_Card (CreditCard#,     CreditCardType, CreditCardExpirationMonth, CreditCardExpirationYear)
CREATE TABLE CREDITCARDS (
                             creditcard_number VARCHAR(16) NOT NULL
    ,creditcard_type VARCHAR(50) NOT NULL
    ,creditcard_expirationmonth SMALLINT NOT NULL
    ,creditcard_expirationyear SMALLINT NOT NULL
    ,PRIMARY KEY(creditcard_number)
);

-- Employee (Employee#, EmployeeSSN, EmployeeName, EmployeeAddress, EmployeeCity, EmployeeState, EmployeeZip, EmployeeHomePhone, EmployeeCellPhone, EmployeeEmail)
CREATE TABLE EMPLOYEES (
                           employee_id BIGINT NOT NULL AUTO_INCREMENT
    ,employee_ssn VARCHAR(20) UNIQUE NOT NULL
    ,employee_name VARCHAR(50) NOT NULL
    ,employee_address VARCHAR(50) NOT NULL
    ,employee_city VARCHAR(50) NOT NULL
    ,employee_state VARCHAR(50) NOT NULL
    ,employee_zipcode VARCHAR(20) NOT NULL
    ,employee_homephone VARCHAR(20) NOT NULL
    ,employee_cellphone VARCHAR(20) NOT NULL
    ,employee_email VARCHAR(50) NOT NULL
    ,employee_commissionpercent DECIMAL(4,2) NOT NULL
    ,employee_salescount INT NOT NULL
    ,employee_salesamount DECIMAL(13,2) NOT NULL
    ,PRIMARY KEY(employee_id)
);

-- Purchase_Agreement (PurchaseAgreement#, PurchaseAgreementDate, PurchaseAgreementTime, PurchaseAgreementPaymentType, PurchaseAgreementCardNumberUsed, Customer#, Employee#)
CREATE TABLE PURCHASEAGREEMENTS(
                                   purchaseagreement_id BIGINT NOT NULL AUTO_INCREMENT
    ,purchase_datetime DATETIME NOT NULL
    ,purchase_paymenttype ENUM('balesofhay','manhours','firstbornchild','creditcard','cash','checks') NOT NULL
    ,purchase_cardnumberused VARCHAR(16) NOT NULL
    ,customer_id BIGINT NOT NULL
    ,employee_id BIGINT NOT NULL
    ,PRIMARY KEY(purchaseagreement_id)
    ,FOREIGN KEY(customer_id)
                                       REFERENCES CUSTOMERS(customer_id)
    ,FOREIGN KEY(employee_id)
                                       REFERENCES EMPLOYEES(employee_id)
);

-- Supplier (Supplier#, SupplierName, SupplierAddress, SupplierCity, SupplierState,     SupplierZip, SupplierPhone, SupplierFax, SupplierContact, SupplierEmail)
CREATE TABLE SUPPLIERS(
                          supplier_id BIGINT NOT NULL AUTO_INCREMENT
    ,supplier_name VARCHAR(50) NOT NULL
    ,supplier_address VARCHAR(50) NOT NULL
    ,supplier_city VARCHAR(50) NOT NULL
    ,supplier_state VARCHAR(50) NOT NULL
    ,supplier_zip  VARCHAR(20) NOT NULL
    ,supplier_phone VARCHAR(20) NOT NULL
    ,supplier_fax VARCHAR(20) NOT NULL
    ,supplier_contact VARCHAR(50) NOT NULL
    ,supplier_email VARCHAR(50) NOT NULL
    ,PRIMARY KEY(supplier_id)
);


-- Product_Category (Model#, ModelDescription, ModelManufacturer, ModelInventoryCount, ModelReorderQuantity)
CREATE TABLE PRODUCTCATEGORIES(
                                  model_id BIGINT NOT NULL AUTO_INCREMENT
    ,model_description VARCHAR(100) NOT NULL
    ,model_manufacturer VARCHAR(50) NOT NULL
    ,model_inventorycount INTEGER NOT NULL
    ,model_reorderquantity INTEGER NOT NULL
    ,PRIMARY KEY(model_id)
);

-- Piece_of_Equipment (Serial#, EquipmentPurchasePrice, EquipmentSalePrice, EquipmentPurchaseDate, Model#, Supplier#) EquipmentType, EquipmentDescription)
CREATE TABLE PIECEOFEQUIPMENT(
                                 equipment_id BIGINT NOT NULL AUTO_INCREMENT
    ,equipment_serialnumber BIGINT NOT NULL
    ,equipment_purchaseprice DECIMAL(13, 2) NOT NULL
    ,equipment_saleprice DECIMAL(13, 2) NOT NULL
    ,equipment_purchasedate DATETIME NOT NULL
    ,model_id BIGINT NOT NULL
    ,supplier_id  BIGINT NOT NULL
    ,PRIMARY KEY(equipment_id)
    ,FOREIGN KEY(model_id)
                                     REFERENCES PRODUCTCATEGORIES(model_id)
    ,FOREIGN KEY(supplier_id)
                                     REFERENCES SUPPLIERS(supplier_id)
);

-- Order (Order#, OrderDatePlaced, OrderDateReceived, Supplier#)
CREATE TABLE ORDERS(
                       order_id BIGINT NOT NULL AUTO_INCREMENT
    ,orders_date_made DATETIME NOT NULL
    ,orders_date_received DATETIME
    ,supplier_id BIGINT NOT NULL
    ,PRIMARY KEY(order_id)
    ,FOREIGN KEY(supplier_id)
                           REFERENCES SUPPLIERS (supplier_id)
);


-- THIS DOESNT HAVE ENOUGH TO IT???
-- Return (Return#, Customer#)
CREATE TABLE RETURNS (
                         return_id BIGINT NOT NULL AUTO_INCREMENT
    ,customer_id BIGINT NOT NULL
    ,PRIMARY KEY(return_id)
    ,FOREIGN KEY(customer_id)
                             REFERENCES CUSTOMERS(customer_id)
);
-- Holds_Account (Customer#, CreditCard#)
CREATE TABLE HOLDS_ACCOUNTS (
                                customer_id BIGINT NOT NULL
    ,creditcard_number VARCHAR(16) NOT NULL
    ,FOREIGN KEY (customer_id)
                                    REFERENCES CUSTOMERS(customer_id)
    ,FOREIGN KEY (creditcard_number)
                                    REFERENCES CREDITCARDS(creditcard_number)
);

-- Line_Item (PurchaseAgreement#, Serial#)
CREATE TABLE LINEITEMS (
                           purchaseagreement_id BIGINT NOT NULL AUTO_INCREMENT
    ,equipment_id BIGINT NOT NULL
    ,FOREIGN KEY (purchaseagreement_id)
                               REFERENCES PURCHASEAGREEMENTS(purchaseagreement_id)
    ,FOREIGN KEY (equipment_id)
                               REFERENCES PIECEOFEQUIPMENT(equipment_id)
);

-- Line_Item_Equipment (Order#, Model#, LineItemEquipmentQuantity, LineItemEquipmentPrice)
CREATE TABLE LINEITEMEQUIPMENTS(
                                   order_id BIGINT NOT NULL
    ,model_id BIGINT NOT NULL
    ,lineitem_equipmentquantity INTEGER NOT NULL
    ,lineitem_equipmentprice DECIMAL(13,2) NOT NULL
    ,FOREIGN KEY(order_id)
                                       REFERENCES ORDERS(order_id)
    ,FOREIGN KEY(model_id)
                                       REFERENCES PRODUCTCATEGORIES(model_id)

);

-- Return_Item(Serial#, Return#, ReturnReason)
CREATE TABLE RETURNITEMS(
                            equipment_id BIGINT NOT NULL
    , return_id BIGINT NOT NULL
    , return_reason VARCHAR(200) NOT NULL
    ,FOREIGN KEY(equipment_id)
                                REFERENCES PIECEOFEQUIPMENT(equipment_id)
    ,FOREIGN KEY(return_id)
                                REFERENCES RETURNS(return_id)
);

USE db4407;

INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer1','Address1','City1','State1','Zipcode1','208111111',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer2','Address2','City2','State1','Zipcode2','208111112',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer3','Address3','City3','State1','Zipcode3','208111113',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer4','Address4','City4','State1','Zipcode4','208111114',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer5','Address5','City5','State1','Zipcode5','208111115',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer6','Address6','City6','State1','Zipcode6','208111116',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer7','Address7','City7','State1','Zipcode7','208111117',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer8','Address8','City8','State1','Zipcode8','208111118',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer9','Address9','City9','State1','Zipcode9','208111119',0.0);
INSERT INTO CUSTOMERS (customer_name, customer_address, customer_city, customer_state, customer_zipcode, customer_phone, customer_creditbalance)
VALUES ('Customer10','Address10','City10','State1','Zipcode10','208111110',0.0);

INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-20');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-21');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-22');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-23');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-24');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-25');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-26');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-27');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-28');
INSERT INTO BALLOONRIDES (balloonride_date)
VALUES ('2019-3-29');

INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(1,1);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(1,2);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(1,3);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(2,1);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(2,2);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(2,3);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(3,1);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(3,2);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(3,3);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(4,1);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(4,2);
INSERT INTO RESERVATIONS (balloonride_id, customer_id)
VALUES(4,3);

INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122223333','DEBIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122224444','DEBIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122225555','DEBIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122226666','DEBIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122227777','DEBIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122228888','CREDIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122229999','CREDIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122220000','CREDIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122221111','CREDIT',2,2020);
INSERT INTO CREDITCARDS (creditcard_number, creditcard_type, creditcard_expirationmonth, creditcard_expirationyear)
VALUES ('0000111122222222','CREDIT',2,2020);

INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-1111','Employee1','Address1','City1','State1','Zipcode1','208111111','2081111111','employee1@gmail.com',2,50,10000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-2222','Employee2','Address1','City1','State1','Zipcode1','208111112','2081111112','employee1@gmail.com',2,50,30000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-3333','Employee3','Address1','City1','State1','Zipcode1','208111113','2081111113','employee1@gmail.com',2,50,50000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-4444','Employee4','Address1','City1','State1','Zipcode1','208111114','2081111114','employee1@gmail.com',2,50,70000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-5555','Employee5','Address1','City1','State1','Zipcode1','208111115','2081111115','employee1@gmail.com',2,50,254300.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-6666','Employee6','Address1','City1','State1','Zipcode1','208111116','2081111116','employee1@gmail.com',2,20,20000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-7777','Employee7','Address1','City1','State1','Zipcode1','208111117','2081111117','employee1@gmail.com',2,60,20000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-8888','Employee8','Address1','City1','State1','Zipcode1','208111118','2081111118','employee1@gmail.com',2,50,20000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-9999','Employee9','Address1','City1','State1','Zipcode1','208111119','2081111119','employee1@gmail.com',2,50,20000.00);
INSERT INTO EMPLOYEES (employee_ssn, employee_name, employee_address, employee_city, employee_state, employee_zipcode, employee_homephone, employee_cellphone, employee_email, employee_commissionpercent, employee_salescount, employee_salesamount)
VALUES ('000-00-0000','Employee10','Address1','City1','State1','Zipcode1','208111110','2081111110','employee1@gmail.com',2,50,20000.00);

INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2010-10-06 12:30:00','creditcard','5268080043376894',1,1);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2010-10-06 12:30:00','cash','5268080043376894',2,1);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',3,1);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','cash','5268080043376894',1,2);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',2,2);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','cash','5268080043376894',3,2);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',1,3);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','cash','5268080043376894',2,3);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',3,3);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','cash','5268080043376894',1,4);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','creditcard','5268080043376894',2,4);
INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2018-05-05 12:30:00','cash','',3,4);

INSERT INTO SUPPLIERS (supplier_name, supplier_address, supplier_city, supplier_state, supplier_zip, supplier_phone, supplier_fax, supplier_contact, supplier_email)
VALUES ('Supplier1','Address1','City1','State1','Zipcode1','208111111','2081111111','Contact1','contact@supplier1.com');
INSERT INTO SUPPLIERS (supplier_name, supplier_address, supplier_city, supplier_state, supplier_zip, supplier_phone, supplier_fax, supplier_contact, supplier_email)
VALUES ('Supplier2','Address2','City1','State1','Zipcode1','208111111','2081111111','Contact2','contact@supplier2.com');
INSERT INTO SUPPLIERS (supplier_name, supplier_address, supplier_city, supplier_state, supplier_zip, supplier_phone, supplier_fax, supplier_contact, supplier_email)
VALUES ('AurSupplier','Address3','City1','State1','Zipcode1','208111111','2081111111','Contact3','contact@supplier3.com');

INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc1','Manu1',10,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc2','Manu1',12,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc3','Manu1',13,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc4','Manu1',14,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc5','Manu1',20,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc6','Aeros',20,4);
INSERT INTO PRODUCTCATEGORIES (model_description, model_manufacturer, model_inventorycount, model_reorderquantity)
VALUES ('Desc7','Aur',20,4);

INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (1234,1000,1200,'2018-03-30',1,1);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (2345,1000,1200,'2018-03-30',2,1);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (3456,1000,1200,'2018-03-30',3,1);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (4567,1000,1200,'2018-03-30',1,2);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (5678,1000,1200,'2018-03-30',2,2);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (6789,1000,1200,'2018-03-30',3,2);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (7890,1000,1200,'2018-03-30',1,3);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (8901,1000,1200,'2018-03-30',2,3);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (9012,1000,1200,'2018-03-30',3,3);
INSERT INTO PIECEOFEQUIPMENT (equipment_serialnumber, equipment_purchaseprice, equipment_saleprice, equipment_purchasedate, model_id, supplier_id)
VALUES (901444,1000,1200,'2016-03-30',3,3);

INSERT INTO ORDERS (orders_date_made, orders_date_received, supplier_id)
VALUES ('2018-03-30','2018-04-05',1);
INSERT INTO ORDERS (orders_date_made, orders_date_received, supplier_id)
VALUES ('2018-03-30','2018-04-05',2);
INSERT INTO ORDERS (orders_date_made, orders_date_received, supplier_id)
VALUES ('2018-03-30','2018-04-05',3);
INSERT INTO ORDERS (orders_date_made, orders_date_received, supplier_id)
VALUES ('2018-03-30',NULL,3);
INSERT INTO ORDERS (orders_date_made, orders_date_received, supplier_id)
VALUES ('2018-03-29',NULL,3);

INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (1,'0000111122221111');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (1,'0000111122222222');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (1,'0000111122223333');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (4,'0000111122224444');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (5,'0000111122225555');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (6,'0000111122226666');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (7,'0000111122227777');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (8,'0000111122228888');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (9,'0000111122229999');
INSERT INTO HOLDS_ACCOUNTS (customer_id, creditcard_number)
VALUES (10,'0000111122220000');

INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (1,1);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (1,2);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (2,3);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (2,4);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (3,5);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (3,6);
INSERT INTO LINEITEMS(purchaseagreement_id,equipment_id)
VALUES (4,7);

INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
VALUES (1,1,4,1000);
INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
VALUES (1,2,4,1000);
INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
VALUES (2,3,4,1000);
INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
VALUES (2,4,4,1000);
INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
VALUES (3,5,4,1000);
# INSERT INTO LINEITEMEQUIPMENTS(order_id,model_id,lineitem_equipmentquantity,lineitem_equipmentprice)
#   VALUES (3,6,4,1000);

INSERT INTO PURCHASEAGREEMENTS (purchase_datetime, purchase_paymenttype, purchase_cardnumberused, customer_id, employee_id)
VALUES ('2010-10-06 12:30:00','creditcard','5268080043376894',1,1);

# noinspection SqlNoDataSourceInspectionForFile

USE db4407;

-- Query 1
-- List the customer name, customer number, purchase agreement number, and purchase total for each purchase agreement.
-- Give the purchase total a meaningful name.
DELIMITER //
CREATE PROCEDURE prQ1()
BEGIN
    SELECT pa.customer_id, c.customer_name, pa.purchaseagreement_id, (SUM(poe.equipment_saleprice)* (1 + 0.05)) AS purchase_total
    FROM PURCHASEAGREEMENTS pa, CUSTOMERS c, LINEITEMS li, PIECEOFEQUIPMENT poe
    WHERE pa.customer_id = c.customer_id
      AND li.purchaseagreement_id = pa.purchaseagreement_id
      AND li.equipment_id = poe.equipment_id
    GROUP BY pa.purchaseagreement_id;
END; //
DELIMITER ;


-- Query 2
-- List the model description and manufacturer for all products whose manufacturer name starts with an "A" and has a third letter "r".
-- (Your answer should include Aeros, Airwave, and Airush.)
-- (When typing this query, copy and paste it into phpMyAdmin, then retype the apostrophe marks)
DELIMITER //
CREATE PROCEDURE prQ2()
BEGIN
    SELECT model_description, model_manufacturer
    FROM PRODUCTCATEGORIES
    WHERE model_manufacturer LIKE 'A_r%';
END; //
DELIMITER ;

-- Query 3
-- List all salesman information, including employee#, name, sales count, and sales total (SalesAmount)
-- and commission earned in descending order by commission earned.
DELIMITER //
CREATE PROCEDURE prQ3()
BEGIN
    SELECT e.employee_id, e.employee_name, e.employee_salescount, e.employee_salesamount, ROUND(e.employee_salesamount * (e.employee_commissionpercent / 100),2) as commission_earned
    FROM EMPLOYEES e
    ORDER BY commission_earned DESC;
END; //
DELIMITER ;

-- Query 4
-- List the customer name, serial number, model number, model description, and purchase agreement date
-- for all items having a purchase agreement date of October 6, 2010.
-- (When typing this query, copy and paste it into phpMyAdmin, then retype the apostrophe marks)
-- TODO: Fix weirdness in phpMyAdmin
DELIMITER //
CREATE PROCEDURE prQ4()
BEGIN
    SELECT c.customer_name, e.equipment_serialnumber, e.model_id, pc.model_description, pa.purchase_datetime
    FROM PURCHASEAGREEMENTS pa, CUSTOMERS c, LINEITEMS li, PIECEOFEQUIPMENT e, PRODUCTCATEGORIES pc
    WHERE pa.customer_id = c.customer_id
      AND li.purchaseagreement_id = pa.purchaseagreement_id
      AND e.equipment_id = li.equipment_id
      AND e.model_id = pc.model_id
      AND DATE(pa.purchase_datetime) = '2010-10-6';

END; //
DELIMITER ;

-- Query 5
-- List all orders that were placed over 1 month ago and have not been received.
-- Include order number, order date placed, order date received, supplier number, supplier name, supplier contact, and supplier phone.
-- List by order date placed from oldest to most recent. INTERVAL is useful here.
DELIMITER //
CREATE PROCEDURE prQ5()
BEGIN
    SELECT o.order_id, o.orders_date_made, o.orders_date_received, s.supplier_id, s.supplier_name, s.supplier_contact, s.supplier_phone
    FROM ORDERS o, SUPPLIERS s
    WHERE o.supplier_id = s.supplier_id
      AND o.orders_date_made <= NOW() - INTERVAL 1 MONTH
      AND o.orders_date_received IS NULL
    ORDER BY o.orders_date_made ASC;
END; //
DELIMITER ;

-- Query 6
-- For each product category list the model number, description, and the total number of sales from that category.
-- As a continuation from the previous query (6a), [using a nested query] list the model number, description,
-- and the total number of sales for the product category that has the highest number of sales. SOMEWHAT HARD
-- Doesn’t account for returns
-- 6a View
DELIMITER //
CREATE VIEW IF NOT EXISTS q6a
AS
SELECT pc.model_id, pc.model_description, COUNT(pe.equipment_id) as total_sales
FROM PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe, LINEITEMS li
WHERE pc.model_id = pe.model_id
  AND pe.equipment_id = li.equipment_id
GROUP BY pc.model_id;
//
DELIMITER ;
-- 6a Query
DELIMITER //
CREATE PROCEDURE prQ6a()
BEGIN
    SELECT * FROM q6a;
END; //
DELIMITER ;

-- 6b Query
DELIMITER //
CREATE PROCEDURE prQ6b()
BEGIN
    SELECT model_id, model_description, total_sales
    FROM q6a
    WHERE total_sales = (SELECT MAX(total_sales) FROM q6a);
END; //
DELIMITER ;


-- Query 7
-- List the model number, description, and number of suppliers for those product categories that were supplied by multiple suppliers. HARD
-- 7 View
DELIMITER //
CREATE VIEW IF NOT EXISTS q7a
AS
SELECT pc.model_id, pc.model_description, COUNT(pe.supplier_id) AS  'supplier_count'
FROM PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe, SUPPLIERS s
WHERE pc.model_id = pe.model_id
  AND pe.supplier_id = s.supplier_id
GROUP BY pc.model_id;
//
DELIMITER ;
# SELECT * FROM q7a;
-- 7 Query
DELIMITER //
CREATE PROCEDURE prQ7()
BEGIN
    SELECT model_id, model_description, supplier_count
    FROM q7a
    WHERE supplier_count > 1;
END; //
DELIMITER ;

-- Query 8
-- Given a customer name, list all credit cards associated with that customer. Include card number, card type (Visa, Mastercard, etc.), and expiration year and month, in the format yyyy/mm. (Arbitrarily select a customer name.)
DELIMITER //
CREATE PROCEDURE prQ8(customer_name VARCHAR(50))
BEGIN
    SELECT c.customer_name, cc.creditcard_number, cc.creditcard_type, (cc.creditcard_expirationyear + '/' + cc.creditcard_expirationmonth) as creditcard_expiration
    FROM CREDITCARDS cc, HOLDS_ACCOUNTS ha, CUSTOMERS c
    WHERE ha.customer_id = c.customer_id
      AND ha.creditcard_number = cc.creditcard_number
      AND c.customer_name = customer_name;
END; //
DELIMITER ;

-- Query 9
-- List all products in inventory that were purchased more than 18 months ago.
-- Display the serial number, purchase date, supplier ID, supplier name, supplier phone number.
DELIMITER //
CREATE PROCEDURE prQ9()
BEGIN
    SELECT pe.equipment_serialnumber, pe.equipment_purchasedate, pe.supplier_id, s.supplier_name, s.supplier_phone
    FROM PIECEOFEQUIPMENT pe, SUPPLIERS s
    WHERE pe.supplier_id = s.supplier_id AND pe.equipment_purchasedate <= (NOW() - INTERVAL 18 MONTH);
END; //
DELIMITER ;

-- Query 10
-- List supplier number and supplier name for all suppliers for which there are no current orders.
-- Sort the list in ascending order by supplier name.
DELIMITER //
CREATE PROCEDURE prQ10()
BEGIN
    SELECT DISTINCT s.supplier_id, s.supplier_name
    FROM SUPPLIERS s
    WHERE (SELECT COUNT(s.supplier_id)
           FROM ORDERS oo
           WHERE oo.supplier_id = s.supplier_id
             AND oo.orders_date_received IS NULL) = 0
    ORDER BY supplier_name;
END; //
DELIMITER ;


use db4407;
# sProc #1
#     modify the Product_Category (or Model) table to include
#       a new Boolean attribute called reorderNecessary
#       that will be set to true when an item needs to be reordered.
#       The attribute should default to false. [do this prior to writing stored procedure]
#     Since the default value may not be valid for all the data in your table, write a stored procedure to set the value of the new reorderNecessary attribute to its correct value.
#         If the modelInventoryCount is less than or equal to the modelReorderQuantity,
#           then set the reorderNecessary value to true.
#         Otherwise set the reorderNecessary value to false.
#         Execute the procedure to reset the value.
ALTER TABLE PRODUCTCATEGORIES
    ADD COLUMN model_reordernecessary BOOL DEFAULT FALSE;

delimiter //
CREATE PROCEDURE prCheckReorderNecessary()
BEGIN
    UPDATE PRODUCTCATEGORIES pc
    SET pc.model_reordernecessary= !(pc.model_inventorycount > pc.model_reorderquantity);
END;//
delimiter ;

# sProc #2
#     The next stored procedure is more complex.
#     When an item is purchased by credit card, the card number must be validated.
#     The algorithm to accomplish this is fairly straightforward, and consists of three steps.
#     These steps are performed by working from the rightmost digit of the credit card number.
#         Step 1: Double the value of alternate digits of the primary account number
#           beginning with the second digit from the right (the right-most digit is the check digit.)
#         Step 2: Still working from the right, add the individual digits comprising
#           the products obtained in Step 1 to each of the unaffected digits in the original number.
#           For example, if the product is 12 then add 1 + 2 to the unaffected digit to the right.
#         Step 3: The total obtained in Step 2 must be a number ending in zero (or mod 10 = 0)
#           for the account number to be validated.
DELIMITER //
CREATE FUNCTION isnumber(inputValue VARCHAR(50))
    RETURNS INT
BEGIN
    IF (inputValue REGEXP ('^[0-9]+$'))
    THEN
        RETURN 1;
    ELSE
        RETURN 0;
    END IF;
END;//
DELIMITER ;

DELIMITER //
CREATE FUNCTION prValidateCreditCard(creditCardNum VARCHAR(16))
    RETURNS INT
BEGIN
    DECLARE counter INT;
    DECLARE summ INT;
    DECLARE number INT;
    DECLARE tmp INT;
    DECLARE result BIT;
    SET result=0;
    IF creditCardNum IS NULL THEN
        RETURN result;
    ELSEIF length(creditCardNum)=0 THEN
        RETURN result;
    END IF;
    SET counter=1;
    WHILE counter<=length(creditCardNum)
    DO
    IF isnumber(substring(creditCardNum,counter,1))=0 THEN
        RETURN (result);
    END IF;
    SET counter=counter+1;
    END WHILE;
    SET summ=0;
    SET number=0;
    SET counter=length(creditCardNum);
    WHILE counter>0
    DO
    IF counter>1 THEN
        SET tmp=(ASCII(substring(creditCardNum,counter-1,1))-48)*2;
        IF tmp>9 THEN
            SET summ=summ+tmp-9;
        ELSE
            SET summ=summ+tmp;
        END IF;
        SET number=number+(ASCII(substring(creditCardNum,counter,1))-48);
        SET counter=counter-2;
    END IF;
    END WHILE;
    SET summ=(summ+number) % 10;
    IF summ=0 THEN
        SET result=1;
    END IF;
    RETURN result;
END; //
DELIMITER ;
use db4407;
# Trigger #1
CREATE TRIGGER sell_inventory AFTER INSERT ON LINEITEMS
    FOR EACH ROW
    UPDATE PRODUCTCATEGORIES pc, PIECEOFEQUIPMENT pe
    SET pc.model_inventorycount = pc.model_inventorycount - 1,
        pc.model_reordernecessary= !(pc.model_inventorycount-1 > pc.model_reorderquantity)
    WHERE NEW.equipment_id = pe.equipment_id
      AND pe.model_id = pc.model_id;

# Trigger #2
delimiter //
CREATE TRIGGER validateCreditCard_Insert BEFORE INSERT ON PURCHASEAGREEMENTS
    FOR EACH ROW
BEGIN
    DECLARE isValid INT;
    SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
    IF isValid = 0 THEN
        # Bad Credit Card
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
    END IF;
END;//
delimiter ;
delimiter //
CREATE TRIGGER validateCreditCard_Update BEFORE UPDATE ON PURCHASEAGREEMENTS
    FOR EACH ROW
BEGIN
    DECLARE isValid INT;
    IF(NEW.purchase_paymenttype = 'creditcard') THEN
        SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
        IF isValid = 0 THEN
            # Bad Credit Card
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
        END IF;
    END IF;
END;//
delimiter ;

# Testing for Validate
delimiter //
CREATE FUNCTION TESTCCTRIGGER(creditCardNum varchar(16))
    RETURNS INT
BEGIN
    DECLARE isValid INT;
    IF(NEW.purchase_paymenttype = 'creditcard') THEN
        SET isValid = prValidateCreditCard(NEW.purchase_cardnumberused);
        IF isValid = 0 THEN
            # Bad Credit Card
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Invalid Credit Card Number";
        END IF;
    END IF;
    RETURN 1;
END;//
delimiter ;