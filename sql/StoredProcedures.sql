use db4407;
# sProc #1
#     modify the Product_Category (or Model) table to include
#       a new Boolean attribute called reorderNecessary
#       that will be set to true when an item needs to be reordered.
#       The attribute should default to false. [do this prior to writing stored procedure]
#     Since the default value may not be valid for all the data in your table, write a stored procedure to set the value of the new reorderNecessary attribute to its correct value.
#         If the modelInventoryCount is less than or equal to the modelReorderQuantity,
#           then set the reorderNecessary value to true.
#         Otherwise set the reorderNecessary value to false.
#         Execute the procedure to reset the value.
ALTER TABLE PRODUCTCATEGORIES
  ADD COLUMN model_reordernecessary BOOL DEFAULT FALSE;

delimiter //
CREATE PROCEDURE prCheckReorderNecessary()
BEGIN
  UPDATE PRODUCTCATEGORIES pc
    SET pc.model_reordernecessary= !(pc.model_inventorycount > pc.model_reorderquantity);
END;//
delimiter ;

# sProc #2
#     The next stored procedure is more complex.
#     When an item is purchased by credit card, the card number must be validated.
#     The algorithm to accomplish this is fairly straightforward, and consists of three steps.
#     These steps are performed by working from the rightmost digit of the credit card number.
#         Step 1: Double the value of alternate digits of the primary account number
#           beginning with the second digit from the right (the right-most digit is the check digit.)
#         Step 2: Still working from the right, add the individual digits comprising
#           the products obtained in Step 1 to each of the unaffected digits in the original number.
#           For example, if the product is 12 then add 1 + 2 to the unaffected digit to the right.
#         Step 3: The total obtained in Step 2 must be a number ending in zero (or mod 10 = 0)
#           for the account number to be validated.
DELIMITER //
CREATE FUNCTION isnumber(inputValue VARCHAR(50))
  RETURNS INT
BEGIN
  IF (inputValue REGEXP ('^[0-9]+$'))
  THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
END;//
DELIMITER ;

DELIMITER //
CREATE FUNCTION prValidateCreditCard(creditCardNum VARCHAR(16))
RETURNS INT
BEGIN
  DECLARE counter INT;
  DECLARE summ INT;
  DECLARE number INT;
  DECLARE tmp INT;
  DECLARE result BIT;
  SET result=0;
  IF creditCardNum IS NULL THEN
    RETURN result;
  ELSEIF length(creditCardNum)=0 THEN
    RETURN result;
  END IF;
  SET counter=1;
  WHILE counter<=length(creditCardNum)
    DO
      IF isnumber(substring(creditCardNum,counter,1))=0 THEN
        RETURN (result);
      END IF;
      SET counter=counter+1;
    END WHILE;
  SET summ=0;
  SET number=0;
  SET counter=length(creditCardNum);
  WHILE counter>0
    DO
      IF counter>1 THEN
        SET tmp=(ASCII(substring(creditCardNum,counter-1,1))-48)*2;
        IF tmp>9 THEN
          SET summ=summ+tmp-9;
        ELSE
          SET summ=summ+tmp;
        END IF;
        SET number=number+(ASCII(substring(creditCardNum,counter,1))-48);
        SET counter=counter-2;
      END IF;
    END WHILE;
  SET summ=(summ+number) % 10;
  IF summ=0 THEN
    SET result=1;
  END IF;
  RETURN result;
END; //
DELIMITER ;
